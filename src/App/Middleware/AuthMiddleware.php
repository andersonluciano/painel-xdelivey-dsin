<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Helpers\GenericGets;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Session\Container;

class AuthMiddleware implements MiddlewareInterface
{

    public $templateRenderer;

    public function __construct(TemplateRendererInterface $templateRenderer)
    {
        $this->templateRenderer = $templateRenderer;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $container = new Container("user_session");


        if ($container->offsetExists("data") && $container->data['id'] != "") {
            $request = $request->withAttribute("session", $container->data);

            if ($container->env == "cliente") {
                try {
                    $cliente = GenericGets::returnLoggedCliente();
                } catch (\Exception $e) {
                    return new RedirectResponse("/login");
                }
            } elseif ($container->env == "admin") {
                try {
                    $usuario = GenericGets::returnLoggedUsuario();
                } catch (\Exception $e) {
                    return new RedirectResponse("/login_admin");
                }
            } else {
                return new RedirectResponse("/login");
            }

            $this->templateRenderer->addDefaultParam(TemplateRendererInterface::TEMPLATE_ALL, 'userSession', $container->data);

            return $handler->handle($request);
        } else {
            return new RedirectResponse("/login");
        }
    }
}
