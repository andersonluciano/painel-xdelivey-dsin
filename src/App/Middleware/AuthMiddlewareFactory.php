<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class AuthMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): AuthMiddleware
    {
        $templateRenderer = $container->get(TemplateRendererInterface::class);

        return new AuthMiddleware($templateRenderer);
    }
}
