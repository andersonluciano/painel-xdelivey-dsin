<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RequestMiddleware implements MiddlewareInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $params = [];

        $json = $request->getBody()->getContents();

        if (!empty($json)) {
            $json = json_decode($json, true);
            if (is_array($json)) {
                $params = array_merge($params, $json);
            }
        }

        $query = $request->getQueryParams();
        if (is_array($query)) {
            $params = array_merge($params, $query);
        }

        $formData = $request->getParsedBody();
        if (is_array($formData)) {
            $params = array_merge($params, $formData);
        }

        $request = $request->withParsedBody($params);

        return $handler->handle($request);
    }
}
