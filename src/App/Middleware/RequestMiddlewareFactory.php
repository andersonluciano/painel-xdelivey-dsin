<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Container\ContainerInterface;

class RequestMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : RequestMiddleware
    {
        return new RequestMiddleware();
    }
}
