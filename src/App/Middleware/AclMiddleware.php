<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Helpers\GenericGets;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Session\Container;

class AclMiddleware implements MiddlewareInterface
{

    public $templateRenderer;

    public function __construct(TemplateRendererInterface $templateRenderer)
    {
        $this->templateRenderer = $templateRenderer;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $container = new Container("user_session");



        if ($container->env != "admin") {
            return new RedirectResponse("/404");
        }

        return $handler->handle($request);
    }
}
