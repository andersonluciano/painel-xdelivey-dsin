<?php

declare(strict_types=1);

namespace App\Middleware;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class AclMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): AclMiddleware
    {
        $templateRenderer = $container->get(TemplateRendererInterface::class);

        return new AclMiddleware($templateRenderer);
    }
}
