<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @mixin \Eloquent
 * @package App
 */
class StatusPedido extends Model
{
    protected $table = "status_pedido";
    protected $primaryKey = "id";
    public $timestamps = null;
}