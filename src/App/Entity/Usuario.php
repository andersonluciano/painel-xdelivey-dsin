<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @mixin \Eloquent
 * @package App
 */
class Usuario extends Model
{
    protected $table = "usuario";
    protected $primaryKey = "id";
    public $timestamps = null;
}