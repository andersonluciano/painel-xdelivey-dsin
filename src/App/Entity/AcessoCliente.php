<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @mixin \Eloquent
 * @package App
 */
class AcessoCliente extends Model
{
    protected $table = "acesso_cliente";
    protected $primaryKey = "id";
    public $timestamps = null;
}