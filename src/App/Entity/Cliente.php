<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @mixin \Eloquent
 * @package App
 */
class Cliente extends Model
{
    protected $table = "cliente";
    protected $primaryKey = "id";
    public $timestamps = null;
}