<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @mixin \Eloquent
 * @package App
 */
class PedidoStatusPedido extends Model
{
    protected $table = "pedido_statuspedido";
    protected $primaryKey = "id";
    public $timestamps = null;
}