<?php
/**
 * Created by PhpStorm.
 * User: desnot01
 * Date: 22/10/18
 * Time: 17:52
 */

namespace App\Helpers;


class DateHandlers
{

    static function returnCreatedAt()
    {
        $date = new \DateTime("now", new \DateTimeZone("America/Sao_Paulo"));

        return $date;
    }

    static function formatDate($dateIn)
    {
        $date = \DateTime::createFromFormat("Y-m-d H:i:sO", $dateIn);

        if (!empty($date)) {
            return $date->format("d/m/Y H:i:s");
        } else {
            return $dateIn;
        }
    }
}