<?php
/**
 * Created by PhpStorm.
 * User: desnot01
 * Date: 22/10/18
 * Time: 17:52
 */

namespace App\Helpers;


class CodeManager
{
    static function returnCode($prefix = "")
    {
        return uniqid($prefix) . rand(1000, 9999);
    }
}