<?php
/**
 * Created by PhpStorm.
 * User: desnot01
 * Date: 22/10/18
 * Time: 17:52
 */

namespace App\Helpers;


use App\Entity\Cliente;
use App\Entity\Pedido;
use App\Entity\PedidoFormaPagamento;
use App\Entity\PedidoItem;
use App\Entity\Usuario;
use Zend\Session\Container;

class GenericGets
{
    /**
     * @return Cliente|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    static function returnLoggedCliente()
    {
        $session = GenericGets::returnLoggedSession();

        $cliente = Cliente::where("id", "=", $session['id'])->first();
        if (empty($cliente)) {
            throw new \Exception("Cliente não encotrado");
        }

        return $cliente;
    }

    static function returnLoggedUsuario()
    {
        $session = GenericGets::returnLoggedSession();

        $usuario = Usuario::where("id", "=", $session['id'])->first();
        if (empty($usuario)) {
            throw new \Exception("Usuário não encotrado");
        }

        return $usuario;
    }

    static function returnLoggedSession()
    {
        $container = new Container("user_session");

        return $container->data;
    }

    static function returnPedidoFull($pedido, $admin = false)
    {

        $pedido = Pedido::
        where("pedido.codigo", "=", $pedido);
        if ($admin == false) {
            $cliente = GenericGets::returnLoggedCliente();
            $pedido->where("pedido.cliente_id", "=", $cliente->id);
        }
        $pedido = $pedido->first();



        if (empty($pedido)) {
            throw new \Exception("Pedido não encontrado");
        }

        $pedidoItem = PedidoItem::
        leftJoin("cardapio as c", "c.id", "=", "pedido_item.cardapio_id")->
        where("pedido_item.pedido_id", "=", $pedido->id)->get(["pedido_item.*", "c.id as item_id", "c.nome as item_nome", "c.preco as item_preco"]);

        $pedidoFormaPagamento = PedidoFormaPagamento::
        leftJoin("forma_pagamento as fp", "fp.id", "=", "pedido_formapagamento.formapagamento_id")->
        where("pedido_formapagamento.pedido_id", "=", $pedido->id)->get(["pedido_formapagamento.id", "fp.codigo", "fp.forma"]);


        $response["pedido"] = $pedido->toArray();

        $response["pedido"]['troco_para'] = "R$ " . str_replace(",", "", $response['pedido']['troco_para']);
        $response["itens"] = $pedidoItem->toArray();
        $response["forma_pagamento"] = $pedidoFormaPagamento->toArray();

        return $response;
    }
}