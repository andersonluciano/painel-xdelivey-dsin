<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Pedido;


use App\Entity\AcessoCliente;
use App\Entity\Cardapio;
use App\Entity\Cliente;
use App\Entity\FormaPagamento;
use App\Entity\Pedido;
use App\Entity\PedidoFormaPagamento;
use App\Entity\PedidoItem;
use App\Entity\PedidoStatusPedido;
use App\Entity\StatusPedido;
use App\Helpers\CodeManager;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Rules\RuleInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class CancelarPedidoRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        $response = [];

        $pedido = Pedido::where("codigo", "=", $data['pedido'])->first();

        $pedidoStatusCheck = PedidoStatusPedido::
        leftJoin("status_pedido as sp", "sp.id", "=", "pedido_statuspedido.statuspedido_id")->
        orderBy("pedido_statuspedido.criado_em", "DESC")->
        where("pedido_statuspedido.pedido_id", "=", $pedido->id)->
        limit(1)->
        first(["sp.codigo"]);
        if ($pedidoStatusCheck->codigo != "AG_INICIO") {
            throw new \Exception("Seu pedido já está em produção e não pode ser cancelado", 418);
        }

        $status = StatusPedido::where("codigo", "=", "CANCELADO")->first();
        if (empty($status)) {
            throw new \Exception("Status do pedido não encontrado", 418);
        }

        Pedido::getConnectionResolver()->connection()->beginTransaction();
        $pedido->finalizado_em = DateHandlers::returnCreatedAt();
        $pedido->save();

        $pedidoStatusPedido = new PedidoStatusPedido();
        $pedidoStatusPedido->pedido_id = $pedido->id;
        $pedidoStatusPedido->statuspedido_id = $status->id;
        $pedidoStatusPedido->criado_em = DateHandlers::returnCreatedAt();
        $pedidoStatusPedido->save();
        Pedido::getConnectionResolver()->connection()->commit();

        $response['pedido'] = $pedido->toArray();
        $response['status'] = $status->toArray();


        return $response;
    }
}