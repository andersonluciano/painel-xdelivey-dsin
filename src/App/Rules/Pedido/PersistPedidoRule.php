<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Pedido;


use App\Entity\AcessoCliente;
use App\Entity\Cardapio;
use App\Entity\Cliente;
use App\Entity\FormaPagamento;
use App\Entity\Pedido;
use App\Entity\PedidoFormaPagamento;
use App\Entity\PedidoItem;
use App\Entity\PedidoStatusPedido;
use App\Entity\StatusPedido;
use App\Helpers\CodeManager;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Rules\RuleInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class PersistPedidoRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        $data['troco'] = trim(str_replace(["R$", ","], ["", "."], $data['troco']));
        if ($data['troco'] == "") {
            $data['troco'] = 0;
        }

        $response = [];
        $cliente = GenericGets::returnLoggedCliente();

        Pedido::getConnectionResolver()->connection()->beginTransaction();

        $status = StatusPedido::where("codigo", "=", "AG_INICIO")->first();
        if (empty($status)) {
            throw new \Exception("Status do pedido não encontrado", 418);
        }

        $pedido = new Pedido();
        $pedido->cliente_id = $cliente->id;
        $pedido->observacoes = $data['observacoes'];
        $pedido->codigo = CodeManager::returnCode("PEDIDO-");
        $pedido->troco_para = $data['troco'];
        $pedido->criado_em = DateHandlers::returnCreatedAt();
        $pedido->save();
        $response['pedido'] = $pedido->toArray();

        $pedidoStatusPedido = new PedidoStatusPedido();
        $pedidoStatusPedido->pedido_id = $pedido->id;
        $pedidoStatusPedido->statuspedido_id = $status->id;
        $pedidoStatusPedido->criado_em = DateHandlers::returnCreatedAt();
        $pedidoStatusPedido->save();

        $response['pedidoStatusPedido'] = $pedidoStatusPedido->toArray();
        $response['pedidoStatusPedido']['status'] = $status->toArray();

        foreach ($data['itens'] as $item) {

            $cardapio = Cardapio::find($item['item']);
            if (empty($cardapio)) {
                throw new \Exception("Item não encontrado", 418);
            }

            $pedidoItem = new PedidoItem();
            $pedidoItem->pedido_id = $pedido->id;
            $pedidoItem->cardapio_id = $cardapio->id;
            $pedidoItem->quantidade = $item['quantidade'];
            $pedidoItem->observacao = "";
            $pedidoItem->criado_em = DateHandlers::returnCreatedAt();
            $pedidoItem->save();

            $response['pedidoItem'][] = $pedidoItem->toArray();
        }

        foreach ($data['formapagamento'] as $item) {
            $formaPagamento = FormaPagamento::where("codigo", "=", $item['codigo'])->first();
            if (empty($formaPagamento)) {
                throw new \Exception("Forma de pagamento não encontrada", 404);
            }

            $pedidoFormaPagamento = new PedidoFormaPagamento();
            $pedidoFormaPagamento->pedido_id = $pedido->id;
            $pedidoFormaPagamento->formapagamento_id = $formaPagamento->id;
            $pedidoFormaPagamento->criado_em = DateHandlers::returnCreatedAt();
            $pedidoFormaPagamento->save();

            $response['pedidoFormaPagamento'][] = $pedidoFormaPagamento->toArray();
        }

        Pedido::getConnectionResolver()->connection()->commit();

        return $response;
    }
}