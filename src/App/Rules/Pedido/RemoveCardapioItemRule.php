<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Pedido;


use App\Entity\AcessoCliente;
use App\Entity\Cardapio;
use App\Entity\Cliente;
use App\Entity\FormaPagamento;
use App\Entity\Pedido;
use App\Entity\PedidoFormaPagamento;
use App\Entity\PedidoItem;
use App\Entity\PedidoStatusPedido;
use App\Entity\StatusPedido;
use App\Helpers\CodeManager;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Rules\RuleInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class RemoveCardapioItemRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        $pedido = GenericGets::returnPedidoFull($data['pedido']);



        $pedidoItem = PedidoItem::where("pedido_id", "=", $pedido['pedido']['id'])->where("id", "=", $data['item'])->first();
        if (empty($pedidoItem)) {
            throw new \Exception("Pedido item não encontrado", 418);
        }
        $pedidoItem->delete();

        return ["status" => "DELETE"];
    }
}