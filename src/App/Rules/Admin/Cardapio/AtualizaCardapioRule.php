<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Admin\Cardapio;


use App\Entity\AcessoCliente;
use App\Entity\Cardapio;
use App\Entity\Cliente;
use App\Entity\FormaPagamento;
use App\Entity\Pedido;
use App\Entity\PedidoFormaPagamento;
use App\Entity\PedidoItem;
use App\Entity\PedidoStatusPedido;
use App\Entity\StatusPedido;
use App\Entity\TipoCardapio;
use App\Helpers\CodeManager;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Rules\RuleInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Zend\Code\Reflection\ParameterReflection;

class AtualizaCardapioRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        $tipoCardapio = TipoCardapio::where("codigo", "=", $data['tipo'])->first();
        if (empty($tipoCardapio)) {
            throw new \Exception("Tipo de cardápio não encontrado", 418);
        }

        $cardapio = Cardapio::where("id", "=", $data['id'])->first();
        if (empty($cardapio)) {
            throw new \Exception("Cardápio não encontrado", 418);
        }
        $cardapio->nome = $data['nome'];
        $cardapio->descricao = $data['descricao'];
        $cardapio->preco = $data['preco'];
        $cardapio->imagem = '';
        $cardapio->tipocardapio_id = $tipoCardapio->id;
        $cardapio->criado_em = DateHandlers::returnCreatedAt();

        $cardapio->save();

        return $cardapio->toArray();
    }
}
