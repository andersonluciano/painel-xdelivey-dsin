<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Admin;


use App\Entity\AcessoCliente;
use App\Entity\AcessoUsuario;
use App\Entity\Cliente;
use App\Entity\Usuario;
use App\Helpers\DateHandlers;
use App\Rules\RuleInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class SigninRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        $email = $data['email'];
        $senha = $data['senha'];

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

            if (!empty(trim($senha))) {

                $usuario = Usuario::where("email", "=", $email)->first();
                if (empty($usuario)) {
                    throw new \Exception("Usuário ou senha inválida", 418);
                }

                $acessoUsuario = AcessoUsuario::
                orderBy("criado_em", "DESC")->
                where("usuario_id", "=", $usuario->id)->first();

                $check = sodium_crypto_pwhash_str_verify(
                    $acessoUsuario->senha,
                    $senha
                );
                if ($check === true) {
                    return $usuario;
                }
                throw new \Exception("Usuário ou senha inválida", 418);


            } else {
                throw new \Exception("Informe sua senha", 418);
            }

        } else {
            throw new \Exception("Email inválido", 418);
        }
    }
}