<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Admin\Pedido;


use App\Entity\AcessoCliente;
use App\Entity\Cardapio;
use App\Entity\Cliente;
use App\Entity\FormaPagamento;
use App\Entity\Pedido;
use App\Entity\PedidoFormaPagamento;
use App\Entity\PedidoItem;
use App\Entity\PedidoStatusPedido;
use App\Entity\StatusPedido;
use App\Helpers\CodeManager;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Rules\RuleInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class RemoveFormaPagamentoRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        $pedido = GenericGets::returnPedidoFull($data['pedido'], true);

        foreach ($pedido['forma_pagamento'] as $item) {
            if ($item['codigo'] == $data['formaPagamento']) {

                $pedidoFormaPagamento = PedidoFormaPagamento::where("id", "=", $item['id']);
                if (empty($pedidoFormaPagamento)) {
                    throw new \Exception("Forma de pagamento não encontrada");
                }
                $pedidoFormaPagamento->delete();
                break;
            }
        }


        return ["status" => "DELETE"];
    }
}