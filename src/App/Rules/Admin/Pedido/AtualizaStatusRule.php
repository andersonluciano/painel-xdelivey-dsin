<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 23/12/18
 * Time: 04:56
 */

namespace App\Rules\Admin\Pedido;

use App\Entity\Pedido;
use App\Entity\PedidoStatusPedido;
use App\Entity\StatusPedido;
use App\Helpers\DateHandlers;
use App\Rules\RuleInterface;


class AtualizaStatusRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        $response = [];

        $pedido = Pedido::where("codigo", "=", $data['pedido'])->first();
        if (empty($pedido)) {
            throw new \Exception("Pedido não encontrado", 418);
        }

        $statusPedido = PedidoStatusPedido::
        leftJoin("status_pedido as sp", "sp.id", "=", "pedido_statuspedido.statuspedido_id")->
        leftJoin("pedido as p", "p.id", "=", "pedido_statuspedido.pedido_id")->
        orderBy("pedido_statuspedido.criado_em", "DESC")->
        limit(1)->
        where("p.id", "=", $pedido->id)->first(["sp.*", "p.criado_em"]);

        if (in_array($statusPedido->codigo, ["CANCELADO", "ENTREGUE"])) {
            throw new \Exception("Pedido já finalizado, seu status não pode ser alterado", 418);
        }

        $status = StatusPedido::where("codigo", "=", $data['status'])->first();
        if (empty($status)) {
            throw new \Exception("Status do pedido não encontrado", 418);
        }


        $pedidoStatusPedido = new PedidoStatusPedido();
        $pedidoStatusPedido->pedido_id = $pedido->id;
        $pedidoStatusPedido->statuspedido_id = $status->id;
        $pedidoStatusPedido->criado_em = DateHandlers::returnCreatedAt();
        $pedidoStatusPedido->save();

        if ($status->codigo == "ENTREGUE") {
            $pedido->finalizado_em = DateHandlers::returnCreatedAt();
            $pedido->save();
        }

        $response['pedido'] = $pedido->toArray();
        $response['status'] = $status->toArray();

        return $response;
    }
}