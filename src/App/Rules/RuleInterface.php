<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:10
 */

namespace App\Rules;


use Psr\Http\Message\ResponseInterface;

interface RuleInterface
{

    /**
     * @param $data
     * @return mixed
     */
    public function run($data);
}