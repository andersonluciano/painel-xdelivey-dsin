<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Sign;


use App\Entity\AcessoCliente;
use App\Entity\Cliente;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Rules\RuleInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class AtualizaClienteRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|void
     */
    public function run($data)
    {
        foreach ($data as $key => $item) {
            if (trim($item) == "") {
                throw new \Exception("Por favor, preencha todos os dados");
            }
        }

        $data['telefone'] = str_replace(["(", ")", "-"], "", $data['telefone']);
        $data['cep'] = str_replace(["-", "."], "", $data['telefone']);

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("Ops, esse email esta em um formato inválido");
        }


        $cliente = GenericGets::returnLoggedCliente();

        $cliente->nome = $data['nome'];
        $cliente->endereco = $data['endereco'];
        $cliente->bairro = $data['bairro'];
        $cliente->numero = $data['numero'];
        $cliente->cep = $data['cep'];
        $cliente->cidade = $data['cidade'];
        $cliente->telefone = $data['telefone'];
        $cliente->email = $data['email'];
        $cliente->save();


        return $cliente;
    }
}