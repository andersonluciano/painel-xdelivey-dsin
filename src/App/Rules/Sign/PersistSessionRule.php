<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Sign;


use App\Entity\AcessoCliente;
use App\Entity\Cliente;
use App\Helpers\DateHandlers;
use App\Rules\RuleInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Session\Container;

class PersistSessionRule implements RuleInterface
{

    /**
     * @param $data
     * @return mixed|Container
     */
    public function run($data)
    {
        $container = new Container("user_session");
        $container->setExpirationSeconds(60 * 60 * 24 * 7);
        $container->data = $data['session'];
        $container->env = $data['env'];

        return $container;
    }
}