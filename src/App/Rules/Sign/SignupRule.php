<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 21/12/18
 * Time: 19:11
 */

namespace App\Rules\Sign;


use App\Entity\AcessoCliente;
use App\Entity\Cliente;
use App\Helpers\DateHandlers;
use App\Rules\RuleInterface;
use Psr\Http\Message\ResponseInterface;

class SignupRule implements RuleInterface
{

    public function run($data)
    {

        $data['telefone'] = str_replace(["(", ")", "-"], "", $data['telefone']);


        $clienteCheck = Cliente::
        where("email", "=", $data['email'])->
        orWhere("telefone", "=", $data['telefone'])->first();
        if (!empty($clienteCheck)) {
            throw new \Exception("Você já possui um cadastro em nosso sistema, por um acaso você esqueceu sua senha?");
        }

        try {


            Cliente::getConnectionResolver()->connection()->beginTransaction();
            $cliente = new Cliente();
            $cliente->nome = $data['nome'];
            $cliente->telefone = $data['telefone'];
            $cliente->email = $data['email'];
            $cliente->criado_em = DateHandlers::returnCreatedAt();
            $cliente->save();

            $acessoCliente = new AcessoCliente();
            $acessoCliente->senha = sodium_crypto_pwhash_str($data['senha'], SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE, SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE);
            $acessoCliente->cliente_id = $cliente->id;
            $acessoCliente->criado_em = DateHandlers::returnCreatedAt();
            $acessoCliente->save();


            Cliente::getConnectionResolver()->connection()->commit();
        } catch (\Exception $e) {
            throw new \Exception("Ocorreu algum probleminha ao tentar criar seu cadastro, poderia tentar novamente em alguns minutos?", 418);
        }


        return $cliente;
    }
}