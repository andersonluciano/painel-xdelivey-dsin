<?php

declare(strict_types=1);

namespace App\Handler;

use App\Rules\Sign\PersistSessionRule;
use App\Rules\Sign\SigninRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Session\Container;

class LoginHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $error = "";
        $email = "";
        if ($request->getMethod() == "POST") {
            $params = $request->getParsedBody();

            $email = $params['email'];
            $senha = $params['senha'];

            $siginRule = new SigninRule();
            /** @var  $cliente */
            try {
                $cliente = $siginRule->run(["email" => $email, "senha" => $senha]);


                $persistSessionRule = new PersistSessionRule();
                $persistSessionRule->run(["session" => $cliente->toArray(), "env" => "cliente"]);

                return new RedirectResponse("/painel");
            } catch (\Exception $e) {
                if ($e->getCode() == 418) {
                    $error = $e->getMessage();
                } else {
                    $error = "Estamos com alguns problemas técnicos, poderia tentar logar em alguns minutos?";
                }
            }
        }

        return new HtmlResponse($this->template->render('app::login', ["email" => $email, "error" => $error]));
    }
}
