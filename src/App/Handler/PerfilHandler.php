<?php

namespace App\Handler;

use App\Helpers\GenericGets;
use App\Helpers\SessionCredentials;
use App\Mirror\OfficeDesk\Profile\Convites;
use App\Rules\Sign\AtualizaClienteRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

class PerfilHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == "POST") {
            return $this->ajax($request);
        }

        try {
            $cliente = GenericGets::returnLoggedCliente();
        } catch (\Exception $e) {
            return new RedirectResponse("/login");
        }

        return new HtmlResponse($this->template->render('app::cadastro-cliente', ["cliente" => $cliente]));
    }

    public function ajax(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        try {
            switch ($params['ajax']) {
                case "salvar":
                    $atualizaClienteRule = new AtualizaClienteRule();
                    $cliente = $atualizaClienteRule->run($params);

                    return new JsonResponse($cliente);
                    break;

            }
        } catch (\Exception $e) {
            return new JsonResponse(["exception" => $e->getMessage()]);
        }

    }
}
