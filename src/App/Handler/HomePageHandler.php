<?php

namespace App\Handler;

use App\Entity\Cardapio;
use App\Entity\Pedido;
use App\Helpers\GenericGets;
use App\Helpers\SessionCredentials;
use App\Mirror\OfficeDesk\Profile\Convites;
use App\Rules\Pedido\CancelarPedidoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

class HomePageHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == "POST") {
            return $this->ajax($request);
        }

        $cliente = GenericGets::returnLoggedCliente();

        $dados = $cliente->toArray();
        foreach ($dados as $key => $inf) {
            if (trim($inf) == "") {
                return new RedirectResponse("/perfil");
            }
        }

        $sql = "SELECT p.*, 
                (
                    SELECT row_to_json(sp.*)FROM pedido_statuspedido psp 
                    LEFT JOIN status_pedido sp ON sp.id = psp.statuspedido_id
                    WHERE psp.pedido_id = p.id
                    ORDER BY psp.criado_em DESC 
                    limit 1
                ) AS status
                FROM pedido p 
                WHERE p.cliente_id = ? AND finalizado_em IS NULL
                ORDER BY p.criado_em DESC";

        $pedidos = Pedido::getConnectionResolver()->connection()->select($sql, [$cliente->id]);

        return new HtmlResponse($this->template->render('app::painel', ["pedidos" => $pedidos]));
    }

    public function ajax(ServerRequestInterface $request)
    {
        $param = $request->getParsedBody();
        try {
            switch ($param['ajax']) {
                case "statusPedido":
                    $cliente = GenericGets::returnLoggedCliente();

                    $sql = "SELECT p.*, 
                            (
                                SELECT row_to_json(sp.*)FROM pedido_statuspedido psp 
                                LEFT JOIN status_pedido sp ON sp.id = psp.statuspedido_id
                                WHERE psp.pedido_id = p.id
                                ORDER BY psp.criado_em DESC 
                                limit 1
                            ) AS status
                            FROM pedido p 
                            WHERE p.cliente_id = ?
                            AND p.codigo = ?
                            ORDER BY p.criado_em DESC";

                    $pedido = Pedido::getConnectionResolver()->connection()->select($sql, [$cliente->id, $param['pedido']]);
                    $pedido = $pedido[0];

//                    $pedido = Pedido::
//                    leftJoin("pedido_statuspedido as psp", "psp.pedido_id", "=", "pedido.id")->
//                    leftJoin("status_pedido as sp", "sp.id", "=", "psp.statuspedido_id")->
//                    orderBy("psp.id", "DESC")->
//                    limit(1)->
//                    where("cliente_id", "=", $cliente->id)->first(["sp.codigo as status_codigo"]);

                    $status = json_decode($pedido->status, 1);
                    $pedido->status_codigo = $status['codigo'];


                    return new JsonResponse($pedido);
                    break;
                case "cancelarPedido":
                    $cancelarPedidoRule = new CancelarPedidoRule();
                    $response = $cancelarPedidoRule->run($param);

                    return new JsonResponse($response);
                    break;

            }
        } catch (\Exception $e) {
            return new JsonResponse(["exception" => $e->getMessage()]);
        }

    }
}
