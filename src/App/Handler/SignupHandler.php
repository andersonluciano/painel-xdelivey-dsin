<?php

namespace App\Handler;

use App\Rules\Sign\PersistSessionRule;
use App\Rules\Sign\SignupRule;
use function Couchbase\passthruEncoder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;


class SignupHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {

        $email = "";
        $nome = "";


        if ($request->getMethod() == "POST") {
            $params = $request->getParsedBody();

            $email = $params['email'];
            $nome = $params['nome'];
            $senha = $params['senha'];


            try {
                if (trim($params['nome']) == "") {
                    throw new \Exception("Informe seu nome", 418);
                }

                if (trim($params['email']) == "") {
                    throw new \Exception("Informe seu email", 418);
                }

                if (trim($params['telefone']) == "") {
                    throw new \Exception("Informe seu telefone", 418);
                }

                if (trim($params['senha']) != trim($params["senha_check"])) {
                    throw new \Exception("As senhas estão divergentes", 418);
                }

                $signupRule = new SignupRule();

                $cliente = $signupRule->run($params);


                if (is_object($cliente) && !empty($cliente->id)) {

                    $persistSessionRule = new PersistSessionRule();
                    $persistSessionRule->run(["session" => $cliente->toArray(), "env" => "cliente"]);

                    return new RedirectResponse("/painel");
                } else {
                    throw new \Exception("", 400);
                }
            } catch (\Exception $e) {

                if ($e->getCode() == 418) {
                    $error = $e->getMessage();
                } else {
                    $error = "Ocorreu algum probleminha ao tentar criar seu cadastro, poderia tentar novamente em alguns minutos?";
                }
            }
        }

        return new HtmlResponse($this->template->render('app::cadastro', ["email" => $email, "nome" => $nome, "error" => $error]));
    }
}
