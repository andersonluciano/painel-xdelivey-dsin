<?php

namespace App\Handler;

use App\Entity\Cardapio;
use App\Entity\FormaPagamento;
use App\Entity\PedidoStatusPedido;
use App\Helpers\GenericGets;
use App\Rules\Pedido\AtualizaPedidoRule;
use App\Rules\Pedido\PersistPedidoRule;
use App\Rules\Pedido\RemoveFormaPagamentoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;


class PedidoDetalheHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $pedido = GenericGets::returnPedidoFull($request->getAttribute("pedido"));

            $statusPedido = PedidoStatusPedido::
            leftJoin("status_pedido as sp", "sp.id", "=", "pedido_statuspedido.statuspedido_id")->
            orderBy("pedido_statuspedido.criado_em", "DESC")->
            limit(1)->
            where("pedido_statuspedido.pedido_id", "=", $pedido['pedido']['id'])->first(["sp.*"]);
        } catch (\Exception $e) {
            return new RedirectResponse("/404");
        }

        return new HtmlResponse($this->template->render('app::pedido-detalhe', ["pedido" => $pedido, "statusPedido" => $statusPedido]));
    }


}
