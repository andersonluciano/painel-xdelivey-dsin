<?php

namespace App\Handler\Admin;

use App\Entity\Cardapio;
use App\Entity\FormaPagamento;
use App\Entity\PedidoStatusPedido;
use App\Helpers\GenericGets;
use App\Rules\Admin\Pedido\AtualizaPedidoRule;
use App\Rules\Admin\Pedido\AtualizaStatusRule;
use App\Rules\Admin\Pedido\RemoveCardapioItemRule;
use App\Rules\Admin\Pedido\RemoveFormaPagamentoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;


class CardapioListHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $cardapios = Cardapio::
        leftJoin("tipo_cardapio as tc", "tc.id", "=", "cardapio.tipocardapio_id")->
        orderBy("cardapio.nome")->get(["cardapio.*", "tc.nome as tipo_nome"]);

        return new HtmlResponse($this->template->render('app::admin/cardapio-list', ["produtos" => $cardapios]));
    }

}
