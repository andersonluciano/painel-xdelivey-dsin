<?php

namespace App\Handler\Admin;

use App\Entity\Cardapio;
use App\Entity\FormaPagamento;
use App\Entity\PedidoStatusPedido;
use App\Entity\TipoCardapio;
use App\Helpers\GenericGets;
use App\Rules\Admin\Cardapio\AtualizaCardapioRule;
use App\Rules\Admin\Cardapio\PersistCardapioRule;
use App\Rules\Admin\Pedido\AtualizaPedidoRule;
use App\Rules\Admin\Pedido\AtualizaStatusRule;
use App\Rules\Admin\Pedido\RemoveCardapioItemRule;
use App\Rules\Admin\Pedido\RemoveFormaPagamentoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;


class CardapioManageHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == "POST") {
            return $this->ajax($request);
        }

        $cardapio = null;
        if ($request->getAttribute("id") != "n") {
            $cardapio = Cardapio::where("id", "=", $request->getAttribute("id"))->first();
            if (empty($cardapio)) {
                return new RedirectResponse("/admin/cardapio");
            }
        }

        $tipoCardapio = TipoCardapio::orderBy("nome")->get();

        return new HtmlResponse($this->template->render('app::admin/cardapio', ["cardapio" => $cardapio, "tipoCardapio" => $tipoCardapio]));
    }

    public function ajax(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        try {
            switch ($params['ajax']) {
                case "gravarCardapio":

                    if ($params['id'] != "") {
                        $atualizaCardapioRule = new AtualizaCardapioRule();
                        $response = $atualizaCardapioRule->run($params);
                    } else {
                        $persistCardapioRule = new PersistCardapioRule();
                        $response = $persistCardapioRule->run($params);
                    }

                    return new JsonResponse($response);
                    break;
            }
        } catch (\Exception $e) {
            return new JsonResponse(["exception" => $e->getMessage()]);
        }
    }
}
