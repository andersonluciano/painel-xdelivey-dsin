<?php

declare(strict_types=1);

namespace App\Handler\Admin;

use App\Entity\AcessoUsuario;
use App\Entity\Usuario;
use App\Helpers\DateHandlers;
use App\Rules\Sign\PersistSessionRule;
use App\Rules\Sign\SigninRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Session\Container;

class LoginHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $error = "";
        $email = "";
        if ($request->getMethod() == "POST") {
            $params = $request->getParsedBody();

            $email = $params['email'];
            $senha = $params['senha'];

            $siginRule = new \App\Rules\Admin\SigninRule();
            /** @var  $usuario */
            try {
                $usuario = $siginRule->run(["email" => $email, "senha" => $senha]);

                $persistSessionRule = new PersistSessionRule();
                $persistSessionRule->run(["session" => $usuario->toArray(), "env" => "admin"]);

                return new RedirectResponse("/admin");
            } catch (\Exception $e) {
                if ($e->getCode() == 418) {
                    $error = $e->getMessage();
                } else {
                    $error = "Estamos com alguns problemas técnicos, poderia tentar logar em alguns minutos?";
                }
            }
        }

        return new HtmlResponse($this->template->render('app::admin/login', ["email" => $email, "error" => $error]));
    }
}
