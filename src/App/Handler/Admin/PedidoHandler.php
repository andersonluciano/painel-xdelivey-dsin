<?php

namespace App\Handler\Admin;

use App\Entity\Cardapio;
use App\Entity\Cliente;
use App\Entity\FormaPagamento;
use App\Entity\PedidoStatusPedido;
use App\Helpers\GenericGets;
use App\Rules\Admin\Pedido\AtualizaPedidoRule;
use App\Rules\Admin\Pedido\AtualizaStatusRule;
use App\Rules\Admin\Pedido\RemoveCardapioItemRule;
use App\Rules\Admin\Pedido\RemoveFormaPagamentoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;


class PedidoHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == "POST") {
            return $this->ajax($request);
        }

        $cardapio = Cardapio::
        leftJoin("tipo_cardapio as tc", "tc.id", "=", "cardapio.tipocardapio_id")->
        orderBy("cardapio.nome")->where("tc.codigo", "=", "LANCHE")->get(["cardapio.*"]);

        $bebidas = Cardapio::
        leftJoin("tipo_cardapio as tc", "tc.id", "=", "cardapio.tipocardapio_id")->
        orderBy("cardapio.nome")->
        where("tc.codigo", "=", "BEBIDA")->get(["cardapio.*"]);

        $formasPagamento = FormaPagamento::all();

        $pedido = "";
        $statusPedido = "";
        $cliente = "";
        if ($request->getAttribute("pedido") != "") {
            $pedido = $request->getAttribute("pedido");

            $statusPedido = PedidoStatusPedido::
            leftJoin("status_pedido as sp", "sp.id", "=", "pedido_statuspedido.statuspedido_id")->
            leftJoin("pedido as p", "p.id", "=", "pedido_statuspedido.pedido_id")->
            orderBy("pedido_statuspedido.criado_em", "DESC")->
            limit(1)->
            where("p.codigo", "=", $pedido)->first(["sp.*", "p.criado_em"]);

            $cliente = Cliente::
            leftJoin("pedido as p", "p.cliente_id", "=", "cliente.id")->
            where("p.codigo", "=", $pedido)->first(["cliente.*"]);
        }


        return new HtmlResponse($this->template->render('app::admin/pedido', ["cardapio" => $cardapio, "bebidas" => $bebidas, "formaPagamento" => $formasPagamento, "pedido" => $pedido, "statusPedido" => $statusPedido, "cliente" => $cliente]));
    }

    public function ajax(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        try {
            switch ($params['ajax']) {
                case "gravarPedido":
                    $atualizaPedidoRule = new AtualizaPedidoRule();
                    $response = $atualizaPedidoRule->run($params);

                    return new JsonResponse($response);
                    break;
                case "loadPedido":
                    $pedido = GenericGets::returnPedidoFull($params['pedido'], true);

                    return new JsonResponse($pedido);
                    break;
                case "removeCardapioItem":
                    $removeCardapioItemRule = new RemoveCardapioItemRule();
                    $response = $removeCardapioItemRule->run($params);

                    return new JsonResponse($response);
                    break;
                case "removeFormaPagamento":

                    $removeFormaPagamentoRule = new RemoveFormaPagamentoRule();
                    $response = $removeFormaPagamentoRule->run($params);

                    return new JsonResponse($response);
                    break;
                case "atualizaStatus":

                    $atualizaStatusRule = new AtualizaStatusRule();
                    $response = $atualizaStatusRule->run($params);

                    return new JsonResponse($response);
                    break;
            }
        } catch (\Exception $e) {
            return new JsonResponse(["exception" => $e->getMessage()]);
        }
    }
}
