<?php

namespace App\Handler\Admin;


use App\Entity\Pedido;
use App\Entity\StatusPedido;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Rules\Admin\Pedido\CancelarPedidoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;

class DashboadHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {

        $statusPedido = StatusPedido::where("codigo", "=", "ENTREGUE")->first();
        if (empty($statusPedido)) {
            throw new \Exception("Status do pedido não encontrado");
        }

        $totalPedidos = Pedido::
        leftJoin("pedido_statuspedido as psp", "psp.pedido_id", "=", "pedido.id")->
        where("psp.statuspedido_id", "=", $statusPedido->id)->
        whereNotNull("finalizado_em")->count(["pedido.*"]);

        $dataFinal = DateHandlers::returnCreatedAt();
        $dataInicial = DateHandlers::returnCreatedAt();
        $dataInicial->sub(new \DateInterval('P7D'));

        $sql = "SELECT 
        SUM(pi.quantidade * c.preco) AS valor_total
        FROM pedido p  
        LEFT JOIN pedido_item pi ON pi.pedido_id = p.id
        LEFT JOIN cardapio c ON pi.cardapio_id = c.id              
        LEFT JOIN cliente cli ON cli.id = p.cliente_id
        where p.criado_em BETWEEN ? AND ?";

        $dataInicial->setTime("0", "0", "0");
        $dataFinal->setTime("23", "59", "59");

        $valorTotal = Pedido::getConnectionResolver()->connection()->select($sql, [$dataInicial, $dataFinal]);
        $valorTotal = $valorTotal[0]->valor_total;


        $sql = "SELECT c.nome,COUNT(cardapio_id) cardapio_item_count 
                FROM pedido_item AS pi
                LEFT JOIN cardapio AS c on c.id = pi.cardapio_id
                GROUP BY cardapio_id,nome
                ORDER BY cardapio_item_count DESC
                LIMIT 1";
        $itemMaisPedido = Pedido::getConnectionResolver()->connection()->select($sql);
        $itemMaisPedido = $itemMaisPedido[0];

        $sql = "SELECT c.nome,COUNT(p.cliente_id) cliente_count 
                FROM pedido AS p
                LEFT JOIN cliente AS c on c.id = p.cliente_id
                GROUP BY cliente_id,c.nome
                ORDER BY cliente_count DESC
                LIMIT 1";
        $topCliente = Pedido::getConnectionResolver()->connection()->select($sql);
        $topCliente = $topCliente[0];

        return new HtmlResponse($this->template->render('app::admin/dashboard', [
            "totalPedidos" => $totalPedidos,
            "valorTotal" => $valorTotal,
            "itemMaisPedido" => $itemMaisPedido,
            "topCliente" => $topCliente
        ]));
    }


}
