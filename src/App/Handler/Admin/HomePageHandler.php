<?php

namespace App\Handler\Admin;


use App\Entity\Pedido;
use App\Helpers\GenericGets;
use App\Rules\Admin\Pedido\CancelarPedidoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;

class HomePageHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {

        if ($request->getMethod() == "POST") {
            return $this->ajax($request);
        }
        $usuario = GenericGets::returnLoggedUsuario();


        $sql = "SELECT p.*, 
                cli.nome,
                (
                    SELECT row_to_json(sp.*) FROM pedido_statuspedido psp 
                    LEFT JOIN status_pedido sp ON sp.id = psp.statuspedido_id
                    WHERE psp.pedido_id = p.id
                    ORDER BY psp.criado_em DESC 
                    limit 1
                ) AS status,
                SUM(pi.quantidade * c.preco) AS valor_total
                FROM pedido p  
                LEFT JOIN pedido_item pi ON pi.pedido_id = p.id
                LEFT JOIN cardapio c ON pi.cardapio_id = c.id              
                LEFT JOIN cliente cli ON cli.id = p.cliente_id              
                WHERE p.finalizado_em IS NULL
                GROUP BY p.id,cli.nome
                ORDER BY p.criado_em DESC";

        $pedidos = Pedido::getConnectionResolver()->connection()->select($sql);

        return new HtmlResponse($this->template->render('app::admin/painel', ["pedidos" => $pedidos]));
    }

    public function ajax(ServerRequestInterface $request)
    {
        $param = $request->getParsedBody();
        try {
            switch ($param['ajax']) {
                case "statusPedido":


                    $sql = "SELECT p.*, 
                            (
                                SELECT row_to_json(sp.*)FROM pedido_statuspedido psp 
                                LEFT JOIN status_pedido sp ON sp.id = psp.statuspedido_id
                                WHERE psp.pedido_id = p.id
                                ORDER BY psp.criado_em DESC 
                                limit 1
                            ) AS status
                            FROM pedido p 
                            WHERE p.codigo = ?
                            ORDER BY p.criado_em DESC";

                    $pedido = Pedido::getConnectionResolver()->connection()->select($sql, [$param['pedido']]);
                    $pedido = $pedido[0];

                    $status = json_decode($pedido->status, 1);
                    $pedido->status_codigo = $status['codigo'];

                    return new JsonResponse($pedido);
                    break;
                case "cancelarPedido":
                    $cancelarPedidoRule = new CancelarPedidoRule();
                    $response = $cancelarPedidoRule->run($param);

                    return new JsonResponse($response);
                    break;

            }
        } catch (\Exception $e) {
            return new JsonResponse(["exception" => $e->getMessage()]);
        }

    }
}
