<?php

namespace App\Handler\Admin;

use App\Entity\Cardapio;
use App\Entity\Pedido;
use App\Helpers\DateHandlers;
use App\Helpers\GenericGets;
use App\Helpers\SessionCredentials;
use App\Mirror\OfficeDesk\Profile\Convites;
use App\Rules\Pedido\CancelarPedidoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;

class HistoricoHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == "POST") {
            return $this->ajax($request);
        }

        $params = $request->getParsedBody();

        if ($params['data-inicio'] == "") {
            $dataFinal = DateHandlers::returnCreatedAt();
            $dataInicial = DateHandlers::returnCreatedAt();
            $dataInicial->sub(new \DateInterval('P10D'));
        } else {
            $dataInicial = \DateTime::createFromFormat("d/m/Y", $params['data-inicio']);
            $dataFinal = \DateTime::createFromFormat("d/m/Y", $params['data-fim']);
        }

        $pedidos = [];
        if (!empty($dataInicial) && !empty($dataFinal)) {
            if ($params['page'] == "") {
                $params['page'] = 1;
            }
            $dataInicial->setTime("0", "0", "0");
            $dataFinal->setTime("23", "59", "59");

            $pedidos = Pedido::
            leftJoin("cliente as c", "pedido.cliente_id", "=", "c.id")->
            whereBetween("pedido.criado_em", [$dataInicial, $dataFinal])->get(["pedido.*", "c.nome as cliente_nome"]);
        } else {
            $error = "Data em formato inválido";
        }

        return new HtmlResponse($this->template->render('app::admin/historico', ["error" => $error, "pedidos" => $pedidos, "dataInicial" => $dataInicial, "dataFinal" => $dataFinal]));
    }
}
