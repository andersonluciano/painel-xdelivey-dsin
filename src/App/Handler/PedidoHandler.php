<?php

namespace App\Handler;

use App\Entity\Cardapio;
use App\Entity\FormaPagamento;
use App\Entity\PedidoStatusPedido;
use App\Helpers\GenericGets;
use App\Rules\Pedido\AtualizaPedidoRule;
use App\Rules\Pedido\PersistPedidoRule;
use App\Rules\Pedido\RemoveCardapioItemRule;
use App\Rules\Pedido\RemoveFormaPagamentoRule;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template\TemplateRendererInterface;


class PedidoHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == "POST") {
            return $this->ajax($request);
        }
        $cliente = GenericGets::returnLoggedCliente();

        $dados = $cliente->toArray();
        foreach ($dados as $key => $inf) {
            if (trim($inf) == "") {
                return new RedirectResponse("/perfil");
            }
        }

        $cardapio = Cardapio::
        leftJoin("tipo_cardapio as tc", "tc.id", "=", "cardapio.tipocardapio_id")->
        orderBy("cardapio.nome")->where("tc.codigo", "=", "LANCHE")->get(["cardapio.*"]);

        $bebidas = Cardapio::
        leftJoin("tipo_cardapio as tc", "tc.id", "=", "cardapio.tipocardapio_id")->
        orderBy("cardapio.nome")->
        where("tc.codigo", "=", "BEBIDA")->get(["cardapio.*"]);

        $formasPagamento = FormaPagamento::all();

        $pedido = "";
        if ($request->getAttribute("pedido") != "") {
            $pedido = $request->getAttribute("pedido");

            $statusPedido = PedidoStatusPedido::
            leftJoin("status_pedido as sp", "sp.id", "=", "pedido_statuspedido.statuspedido_id")->
            leftJoin("pedido as p", "pedido_statuspedido.pedido_id", "=", "p.id")->
            orderBy("pedido_statuspedido.criado_em", "DESC")->
            limit(1)->
            where("p.codigo", "=", $pedido)->first(["sp.*"]);

            if ($statusPedido->codigo != "AG_INICIO") {
                return new RedirectResponse("/pedido-detalhe/" . $pedido);
            }
        }


        return new HtmlResponse($this->template->render('app::cardapio', ["cardapio" => $cardapio, "bebidas" => $bebidas, "formaPagamento" => $formasPagamento, "pedido" => $pedido]));
    }

    public function ajax(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        try {
            switch ($params['ajax']) {
                case "gravarPedido":
                    if ($params['pedido'] != "") {
                        $atualizaPedidoRule = new AtualizaPedidoRule();
                        $response = $atualizaPedidoRule->run($params);
                    } else {
                        $persistPedidoRule = new PersistPedidoRule();
                        $response = $persistPedidoRule->run($params);
                    }


                    return new JsonResponse($response);
                    break;
                case "loadPedido":
                    $pedido = GenericGets::returnPedidoFull($params['pedido']);

                    return new JsonResponse($pedido);
                    break;
                case "removeCardapioItem":
                    $removeCardapioItemRule = new RemoveCardapioItemRule();
                    $response = $removeCardapioItemRule->run($params);

                    return new JsonResponse($response);
                    break;
                case "removeFormaPagamento":

                    $removeFormaPagamentoRule = new RemoveFormaPagamentoRule();
                    $response = $removeFormaPagamentoRule->run($params);

                    return new JsonResponse($response);
                    break;
            }
        } catch (\Exception $e) {
            return new JsonResponse(["exception" => $e->getMessage()]);
        }
    }
}
