<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;

/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container): void {
    $app->route('/', App\Handler\LoginHandler::class, ["GET"], 'home');
    $app->route('/painel', App\Handler\HomePageHandler::class, ["GET", "POST"], 'painel');
    $app->route('/perfil', App\Handler\PerfilHandler::class, ["GET", "POST"], 'perfil');
    $app->route('/pedido[/{pedido}]', App\Handler\PedidoHandler::class, ["GET", "POST"], 'pedido');
    $app->route('/pedido-detalhe/{pedido}', App\Handler\PedidoDetalheHandler::class, ["GET"], 'pedido-detalhe');
    $app->route('/historico', App\Handler\HistoricoHandler::class, ["GET"], 'historico');

    $app->route('/login', \App\Handler\LoginHandler::class, ["GET", "POST"], 'login');
    $app->route('/logout', \App\Handler\LogoutHandler::class, ["GET"], 'logout');
    $app->route('/esqueci-senha', \App\Handler\LoginHandler::class, ["GET", "POST"], 'esqueci-senha');
    $app->route('/cadastrar-se', \App\Handler\SignupHandler::class, ["GET", "POST"], 'cadastrar-se');

    $app->route('/login_admin', \App\Handler\Admin\LoginHandler::class, ["GET", "POST"], 'login-admin');
    $app->route('/admin', \App\Handler\Admin\HomePageHandler::class, ["GET", "POST"], 'admin');
    $app->route('/admin/pedido[/{pedido}]', \App\Handler\Admin\PedidoHandler::class, ["GET", "POST"], 'admin.pedido');
    $app->route('/admin/historico', \App\Handler\Admin\HistoricoHandler::class, ["GET", "POST"], 'admin.historico');
    $app->route('/admin/cardapio', \App\Handler\Admin\CardapioListHandler::class, ["GET"], 'admin.cardapio');
    $app->route('/admin/cardapio/{id}', \App\Handler\Admin\CardapioManageHandler::class, ["GET", "POST"], 'admin.cardapio-manage');
    $app->route('/admin/dashboard', \App\Handler\Admin\DashboadHandler::class, ["GET", "POST"], 'admin.dashboard');
};
