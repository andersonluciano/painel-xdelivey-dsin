<?php


return [
    'eloquent' => [
        'driver'    => 'pgsql',
        'host'      => 'localhost',
        'database'  => 'xdelivery',
        'username'  => 'postgres',
        'password'  => '123456',
        'charset'   => 'utf8',
        'collation' => 'utf8_general_ci',
    ]
];