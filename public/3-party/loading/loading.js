var loading = '<div class="col-md-12 text-center">' +
    '<label style="color:#d96c35">Valbernielson\' Hamburgueria  Pedidos</label>' +
    '<div class="sk-folding-cube">' +
    '<div class="sk-cube1 sk-cube"></div>' +
    '<div class="sk-cube2 sk-cube"></div>' +
    '<div class="sk-cube4 sk-cube"></div>' +
    '<div class="sk-cube3 sk-cube"></div>' +
    '</div>' +
    '</div>';

function addLoading(elem) {
    table = $(elem).parents("table");
    if (table.length > 0) {
        var len = $(table).find("th").length;
        if (len > 0) {
            var tr = $("<tr>");
            var td = $("<td colspan='" + len + "'>");

            $(td).append(loading);

            $(tr).append(td);

            $(elem).html(tr);

            return;
        }
    }

    $(elem).html(loading);
}


function blockUI(label) {
    if (label != undefined) {
        $(".preloader-it").find("label").html(label);
    }
    $(".preloader-it").fadeIn("fast");
}

function unblockUI() {
    $(".preloader-it").fadeOut("fast");
}