$(function () {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).load(function () {
        addLoading($(".preloader-it"));
    });

    $(window).bind("load resize", function () {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }

        $(".preloader-it").delay(500).fadeOut("fast");
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function () {
        return this.href == url;
    }).addClass('active').parent().parent().addClass('in').parent();
    console.log(element);
    if (element.is('li')) {
        element.addClass('active');
    }
});


function date_format(date) {
    mom = moment(date);

    return mom.format("D-M-YYYY HH:mm");
}

function addDatePicker(input) {
    console.log("hey");
    console.log(input);
    $(input).datepicker({
        language: 'pt-BR',
        autoclose: true,
        todayBtn: true,
        todayHighlight: true
    })
    ;
}

function inputInvalid(input) {
    $(input).parents(".form-group").addClass("has-error");
    $(input).change(function () {
        if ($(this).val() != "") {
            $(input).parents(".form-group").removeClass("has-error");
        } else {
            $(input).parents(".form-group").addClass("has-error");
        }
    });
}