Dependencias
	- PHP Mínimo 7.0
	- php-pgsql
	- php-json
	- php-libsodium - Já vem na versão 7.2, caso contrário é necessário instalar ou compilar

Banco PostgreSQL

Deixei um backup do banco com dados pré-inseridos na pasta database, juntamente um diagrama da modelagem

Utilizando o pg_admin:
	- Criar um banco de dados
	- Acessar a opção restore escolher o arquivo e restaurar

Via pg_dump
	- /usr/bin/pg_restore --host localhost --port 5432 --username "postgres" --dbname "nomedobanco" --no-password  --verbose "/CAMINHO_ATE/bkp.backup"

No projeto pasta:
	- config/autoload/eloquent.global.php.dist
	- Copiar e colar o arquivo tirando o .dist e alterar os parâmetros de conexão para o banco instalado

Baixar o composer e rodar o composer install na pasta raiz do projeto

Entrar na pasta public e rodar o servidor local do PHP
	- php -S localhost:8000
	ou
	- Configurar Apache/Nginx apontando todo roteamento para a pasta public

Acessos:

Administrador URL -> /login_admin
U: megusta@teste.com
S: 123456

Cliente  URL -> /login

Usuário 1
U: teste@teste.com
S: 123456

Usuário 2
U: teste2@teste.com
S: 123456

Caso queiram cadastrar um novo cliente a opção de cadastrar-se está disponível











